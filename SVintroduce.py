import argparse
import tools
import motif
import threeDgenome


parser = argparse.ArgumentParser()
parser.add_argument('segments', help="BED file containing division of the "
                                     "genome into topological domains")
parser.add_argument('interactions', help="BEDPE file containing genomic "
                                         "interactions - format: chr1 st1 "
                                         "end1 chr2 st2 end2 orient1 orient2 "
                                         "score. Were orient stands for "
                                         "orientation of the protein factor "
                                         "binding motif and accepted values "
                                         "are: R - right (positive strand), "
                                         "L - left (negative strand), N - "
                                         "non-specified"
                    )
parser.add_argument('chrom_lengths', help="BED file containing chromosome "
                                          "lengths")
parser.add_argument('svs', help="VCF file containing SVs to be introduced "
                                "into the 3D genome")
parser.add_argument('-m', '--PFM', help="File defining a Position Frequency "
                                        "Matrix (PWM)")
parser.add_argument('-s', '--sampleID', help="ID of a sample listed in the "
                                             "provided VCF file which "
                                             "genotype should be extracted")
parser.add_argument('-a', '--allele', type=int, choices=[0, 1], default=0,
                    help="Number specifying the allele of a sample which "
                         "should be considered. If sample's genotype is 1|0, "
                         "-a 0 indicates left (paternal) allele, i.e. 1 and "
                         "-a 1 indicates right (maternal) allele, i.e. 0. "
                         "Default 0")
parser.add_argument('-x', '--suffix', default='mod', help="Suffix that will "
                                                          "be added to the "
                                                          "output files")
args = parser.parse_args()

segments = tools.read_bed(args.segments)
clusters = tools.read_bed(args.interactions)
chr_lengths_dict = dict(tools.read_bed(args.chrom_lengths))
if args.PFM:
    pfm = motif.read_raw_pfm(args.PFM)
    pwm = motif.pfm_to_pwm(pfm)
else:
    pwm = None

print "Constructing the genome..."
genome = threeDgenome.Genome(segments, clusters, chr_lengths_dict, pwm)
print "Done."

print "Introducing SVs..."
genome.map_svs_from_vcf(args.svs, sample_name=args.sampleID,
                        alt_allele=args.allele)
print "Done."

print "Extracting the data..."
segment_boundaries = genome.get_segment_boundaries()
new_clusters = genome.get_interactions()
new_anchors = [[tuple(line[:3] + [line[6]]), tuple(line[3:6] + [line[7]])]
               for line in new_clusters]
new_anchors = set((elem for sublist in new_anchors for elem in sublist))
new_anchors_merged = tools.merge_anchors(new_anchors)
new_clusters_modeling = (line[:6] + [line[8]] + [0, 0] for line in
                         new_clusters)
new_chr_lengths = genome.chr_lengths_dict.items()
print "Done."
segment_boundaries_filename = tools.add_suffix(args.segments, args.suffix)
new_clusters_filename = tools.add_suffix(args.interactions, args.suffix)
new_anchors_filename = '.'.join(tools.add_suffix(tools.add_suffix(
    args.interactions, "merged_anchors"), args.suffix).split('.')[:-1]) + \
                       ".bed"
new_chr_lengths_filename = tools.add_suffix(args.chrom_lengths, args.suffix)

print "Saving to files..."
tools.save_as_tsv(segment_boundaries, segment_boundaries_filename, sep=' ')
tools.save_as_tsv(new_clusters_modeling, new_clusters_filename)
tools.save_as_tsv(new_anchors_merged, new_anchors_filename)
tools.save_as_tsv(new_chr_lengths, new_chr_lengths_filename)
print "Done."
